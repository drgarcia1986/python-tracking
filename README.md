﻿python-tracking
===============

python module for tracking mail packages

**Mail service list**
* USPS (United States)
* Correios (Brazil)
* Hong Kong Post (Hong Kong)

**C.I. Status**
------------
[![Build Status](https://travis-ci.org/drgarcia1986/python-tracking.png)](http://travis-ci.org/drgarcia1986/python-tracking)
[![Coverage Status](https://coveralls.io/repos/drgarcia1986/python-tracking/badge.png)](https://coveralls.io/r/drgarcia1986/python-tracking)

**Install:**
```python
python setup.py install
```

**Example of using:**
```python
import ptracking


# The argument of constructor is your current location
t = ptracking.PyTracking(ptracking.BRAZIL)
print(t.tracking('LZ428144337US'))
```

**Example of Result:**
```python
{'tracking': [{'observation': 'Em trânsito para UNIDADE DE TRATAMENTO INTERNACIONAL - BRASIL', 'time': '28/02/2014 07:05 AM', 'status': 'Encaminhado', 'location': 'ESTADOS UNIDOS'}, {'time': '24/02/2014 10:26 AM', 'status': 'Postado', 'location': 'ESTADOS UNIDOS'}, {'time': '28/02/2014 07:05 AM', 'status': 'Processed Through Sort Facility', 'location': 'ISC MIAMI FL USPS'}, {'time': '28/02/2014', 'status': 'Depart USPS Sort Facility', 'location': 'MIAMI FL 33112'}, {'time': '27/02/2014 03:12 PM', 'status': 'Processed through USPS Sort Facility', 'location': 'MIAMI FL 33112'}, {'time': '27/02/2014 03:12 PM', 'status': 'Arrived at Sort Facility', 'location': 'ISC MIAMI FL USPS'}, {'observation': 'Electronic Shipping Info Received', 'time': '24/02/2014 10:26 AM', 'status': 'Acceptance', 'location': 'TAMPA FL 33603'}]}
```

**With json dumps:**
```python
import ptracking
import json


# The argument of constructor is your current location
t = ptracking.PyTracking(ptracking.BRAZIL)
print(json.dumps(t.tracking('LZ428144337US'), indent=3, ensure_ascii=False))
```

**get this result:**
```json
{
   "tracking": [
      {
         "time": "28/02/2014 07:05 AM",
         "status": "Encaminhado",
         "observation": "Em trânsito para UNIDADE DE TRATAMENTO INTERNACIONAL - BRASIL",
         "location": "ESTADOS UNIDOS"
      },
      {
         "time": "24/02/2014 10:26 AM",
         "status": "Postado",
         "location": "ESTADOS UNIDOS"
      },
      {
         "time": "28/02/2014 07:05 AM",
         "status": "Processed Through Sort Facility",
         "location": "ISC MIAMI FL USPS"
      },
      {
         "time": "28/02/2014",
         "status": "Depart USPS Sort Facility",
         "location": "MIAMI FL 33112"
      },
      {
         "time": "27/02/2014 03:12 PM",
         "status": "Processed through USPS Sort Facility",
         "location": "MIAMI FL 33112"
      },
      {
         "time": "27/02/2014 03:12 PM",
         "status": "Arrived at Sort Facility",
         "location": "ISC MIAMI FL USPS"
      },
      {
         "time": "24/02/2014 10:26 AM",
         "status": "Acceptance",
         "observation": "Electronic Shipping Info Received",
         "location": "TAMPA FL 33603"
      }
   ]
}
```


Same of:
**USPS**

![USPS](/img/usps.PNG)

and
**Correios**

![correios](/img/correios.PNG)

### TODO

- [ ] Tracking for China Post
- [ ] Version 1.1 on PyPi
