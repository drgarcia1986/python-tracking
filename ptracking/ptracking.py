__author__ = 'Diego Garcia'

from .ptracking_utils import PyTrackingBR, PyTrackingUSPS, PyTrackingHK

BRAZIL = 'BR'
UNITED_STATES = 'US'
HONK_KONG = 'HK'


class ELocationNotImplemented(Exception):
    def __str__(self):
        return 'Location "{}" not implemeted'.format(self.args[0])


class PyTracking:

    __tracking_methods = {BRAZIL: PyTrackingBR.get_tracking,
                          UNITED_STATES: PyTrackingUSPS.get_tracking,
                          HONK_KONG: PyTrackingHK.get_tracking}

    def __init__(self, my_location=None):
        self.my_location = my_location

    def __tracking_method_by_location(self, location):
        try:
            return self.__tracking_methods[location]
        except KeyError:
            raise ELocationNotImplemented(location)

    def __extend_tracking(self, location, cod, data):
        m = self.__tracking_method_by_location(location)
        if m:
            data["tracking"].extend(m(cod))
        return True

    def tracking(self, cod):
        data = {"tracking": []}
        self.__extend_tracking(self.my_location, cod, data)
        if self.my_location != cod[-2:].upper():
            self.__extend_tracking(cod[-2:].upper(), cod, data)

        return data