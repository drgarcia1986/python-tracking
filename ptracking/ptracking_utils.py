__author__ = 'Diego Garcia'

import re
from datetime import datetime

try:
    import urllib.request as req
except ImportError:
    import urllib2 as req


class PyTrackingUtils:
    @staticmethod
    def get_html(url):
        page = req.urlopen(url)
        return page.read().decode("ISO-8859-1")

    @staticmethod
    def clean_up_string(s):
        return re.sub(r'\W+', ' ', s, re.DOTALL).strip()

    @staticmethod
    def clean_up_tags(s):
        cs = re.sub(r'<[^>]+>', '', s, re.DOTALL).strip()
        return PyTrackingUtils.clean_up_string(cs)

    @staticmethod
    def convert_to_12h_time_format(time):
        dt = datetime.strptime(time, "%d/%m/%Y %H:%M")
        return dt.strftime("%d/%m/%Y %I:%M %p")


class PyTrackingBR:
    @staticmethod
    def get_tracking(cod):
        data = []
        s = PyTrackingUtils.get_html('http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_ITEMCODE=&'
                                     'P_LINGUA=001&P_TESTE=&P_TIPO=001&P_COD_UNI={}&Z_ACTION=Search'.format(cod))
        p_re = r'rowspan=\d+>(?P<time>[^<]+)</td><td>(?P<location>[^<]+)</td><td><FONT COLOR="[A-F\d]{6}">' \
               r'(?P<status>[^<]+)|colspan=\d+>(?P<observation>[^<]+?)</'
        for m in re.finditer(p_re, s, re.DOTALL + re.UNICODE):
            if m.group('time') is not None:
                data.append({"time": PyTrackingUtils.convert_to_12h_time_format(m.group('time')),
                             "location": m.group('location'),
                             "status": m.group('status')})
            else:
                data[-1]["observation"] = m.group('observation')
        return data


class PyTrackingHK:
    @staticmethod
    def __format_date(date):
        dt = datetime.strptime(date, "%d-%b-%Y")
        return dt.strftime("%d/%m/%Y")

    @staticmethod
    def get_tracking(cod):
        data = []
        s = PyTrackingUtils.get_html('http://app3.hongkongpost.hk/CGI/mt/genresult.jsp?tracknbr={}'.format(cod))
        p_re = r'.*(?P<observation>Destination</span> - \w+)</p>.*The item \(\w{13}\) (?P<status>[a-zA-Z ]+) on ' \
               r'(?P<time>[\w-]+)\.?'
        m = re.match(p_re, s, re.DOTALL)
        if m:
            data.append({"time": PyTrackingHK.__format_date(m.group('time')),
                         "status": m.group('status'),
                         "observation": PyTrackingUtils.clean_up_tags(m.group('observation'))})
        return data


class PyTrackingUSPS:
    @staticmethod
    def __format_date_time(time):
        m = re.match(r'\w+\s\d{1,2},\s\d{4},\s\d{1,2}:\d{1,2}(\s[AP]M|:\d{1,2})', time)
        if m:
            if m.group(1)[-1] == 'M':
                f = '%B %d, %Y, %I:%M %p'
            else:
                f = '%B %d, %Y, %H:%M:%S'
            dt = datetime.strptime(time, f)
            return dt.strftime("%d/%m/%Y %I:%M %p")
        else:
            dt = datetime.strptime(time, '%B %d, %Y, ')
            return dt.strftime("%d/%m/%Y")

    @staticmethod
    def get_tracking(cod):
        data = []
        s = PyTrackingUtils.get_html('http://www.stamps.com/shipstatus/submit/?confirmation={}'.format(cod))
        p_re = r'(?s)class="scanHistoryLeftCol">(?P<time>[^<]*).*?<td>(?P<time_detail>[^<]*).*?<td>(?P<location>' \
               r'[^<]*).*?ol">(?P<status>[^<]*)'
        obs = ""
        for m in re.finditer(p_re, s, re.DOTALL):
            if m.group('location').strip() != "":
                data.append({"time": PyTrackingUSPS.__format_date_time('{}, {}'.format(m.group('time'),
                                                                                       m.group('time_detail').upper())),
                             "location": PyTrackingUtils.clean_up_string(m.group('location')),
                             "status": m.group('status')})
                if obs != "":
                    data[-1]["observation"] = obs
                    obs = ""
            elif len(data) == 0:
                data.append({"time": PyTrackingUSPS.__format_date_time('{}, {}'.format(m.group('time'),
                                                                                       m.group('time_detail').upper())),
                             "status": m.group('status')})
            else:
                obs = m.group('status')
        return data
