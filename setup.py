__author__ = 'Diego Garcia'


from distutils.core import setup


setup(
    name            =   'python-tracking',
    version         =   '1.1.0',
    py_modules      =   ['ptracking.__init__'],
    author          =   'Diego Garcia',
    author_email    =   'drgarcia1986@gmail.com',
    url             =   'https://github.com/drgarcia1986/python-tracking',
    description     =   'python module for tracking mail packages',
    classifiers     =   [
        "Intended Audience :: Developers",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Topic :: Internet",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ])