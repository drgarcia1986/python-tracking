#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

from ptracking import ptracking as pt
from tests.utils import unittest
from tests.utils import mock


class MockTracking:
    tmp_list = []

    @staticmethod
    def get_tracking(cod):
        return MockTracking().tmp_list


class TestPyTracking(unittest.TestCase):
    def setUp(self):
        self.t = pt.PyTracking()
        self.cod_for_tracking = "LZ428144337US"
        MockTracking.tmp_list = []

    def test_tracking_method_by_location(self):
        method = self.t._PyTracking__tracking_method_by_location
        self.assertIs(method(pt.BRAZIL), pt.PyTrackingBR.get_tracking)
        self.assertIs(method(pt.UNITED_STATES), pt.PyTrackingUSPS.get_tracking)
        self.assertIs(method(pt.HONK_KONG), pt.PyTrackingHK.get_tracking)
        self.assertRaises(pt.ELocationNotImplemented, method, "CH")
        try:
            self.assertRaisesRegex(pt.ELocationNotImplemented, 'Location "CH" not implemeted', method, "CH")
        except AttributeError:
            self.assertRaisesRegexp(pt.ELocationNotImplemented, 'Location "CH" not implemeted', method, "CH")


    @mock.patch("ptracking.ptracking.PyTracking._PyTracking__tracking_method_by_location", autospec=True)
    def test_extend_tracking(self, mock_get_method):
        method = self.t._PyTracking__extend_tracking

        first_tracking = {'time': '24/02/2014 10:26 AM',
                          'status': 'Acceptance',
                          'location': 'TAMPA FL 33603'}
        data = {"tracking": [first_tracking]}

        second_tracking = {'time': '24/02/2014 10:26 AM',
                           'status': 'Postado',
                           'location': 'ESTADOS UNIDOS'}

        mock_get_method.return_value = MockTracking.get_tracking
        MockTracking.tmp_list.append(second_tracking)

        self.assertTrue(method(pt.BRAZIL, self.cod_for_tracking, data))
        self.assertListEqual([first_tracking, second_tracking], data["tracking"])

        mock_get_method.assert_called_once_with(self.t, pt.BRAZIL)

    @mock.patch("ptracking.ptracking.PyTracking._PyTracking__tracking_method_by_location", autospec=True)
    def test_tracking(self, mock_get_method):
        mock_get_method.return_value = MockTracking.get_tracking
        self.t.my_location = pt.UNITED_STATES

        self.assertDictEqual({"tracking": []}, self.t.tracking(self.cod_for_tracking))

        simple_tracking = {'time': '24/02/2014 10:26 AM',
                           'status': 'Postado',
                           'location': 'ESTADOS UNIDOS'}
        MockTracking.tmp_list.append(simple_tracking)
        self.assertDictEqual({"tracking": [simple_tracking]}, self.t.tracking(self.cod_for_tracking))

    @mock.patch("ptracking.ptracking.PyTracking._PyTracking__tracking_method_by_location", autospec=True)
    def test_tracking_with_different_origin_and_destination(self, mock_get_method):
        mock_get_method.return_value = MockTracking.get_tracking
        self.t.my_location = pt.BRAZIL

        simple_tracking = {'time': '24/02/2014 10:26 AM',
                           'status': 'Postado',
                           'location': 'ESTADOS UNIDOS'}
        MockTracking.tmp_list.append(simple_tracking)
        self.assertListEqual([simple_tracking, simple_tracking], self.t.tracking(self.cod_for_tracking)["tracking"])


if __name__ == '__main__':
    unittest.main()
