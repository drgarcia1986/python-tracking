#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

from tests.utils import unittest
from tests.utils import mock
from ptracking.ptracking_utils import PyTrackingBR


class TestPyTrackingBR(unittest.TestCase):
    def setUp(self):
        self.patcher = mock.patch('ptracking.ptracking_utils.PyTrackingUtils.get_html')
        self.mock_http = self.patcher.start()
        self.cod_for_tracking = "LZ428144337US"

    def tearDown(self):
        self.patcher.stop()

    def test_simple_tracking(self):
        s = '<table  border cellpadding=1 hspace=10>' \
            '<colgroup style=\'font:8pt Tahoma;color=Black\' valign=top><colgroup style=\'font:8pt Tahoma; color=Navy\'><colgroup style=\'font:8pt Tahoma;color=Maroon\'>' \
            '<tr>' \
            '<td><font FACE=Tahoma color=\'#CC0000\' size=2><b>Data</b></font></td>' \
            '<td><font FACE=Tahoma color=\'#CC0000\' size=2><b>Local</b></font></td>' \
            '<td><font FACE=Tahoma color=\'#CC0000\' size=2><b>Situação</b></font></td>' \
            '</tr>' \
            '<tr><td rowspan=1>24/02/2014 10:26</td><td>ESTADOS UNIDOS</td><td><FONT COLOR="000000">Postado</font></td></tr>' \
            '</TABLE>'

        self.mock_http.return_value = s
        result = [{'time': '24/02/2014 10:26 AM', 'status': 'Postado', 'location': 'ESTADOS UNIDOS'}]
        self.assertListEqual(result, PyTrackingBR.get_tracking(self.cod_for_tracking))

    def test_empty_tracking(self):
        s = '<BODY BGCOLOR="d8e6ed" TEXT="0000FF" LINK="000000"VLINK="800040" ALINK="0FF000"> ' \
            '<FORM ACTION="txect01$.ErrorMsg" METHOD="POST"> ' \
            '<H1> </H1> ' \
            '<img align=absmiddle src=../correios/Img/correios.gif>' \
            '<FONT face=Arial size=3 color="#000000"><b>Resultado da Pesquisa</b></font>' \
            '<br><br>' \
            '<FONT face=Arial size=2 color=black><b></b><p>' \
            'O nosso sistema não possui dados sobre o objeto' \
            'informado. Se o objeto foi postado recentemente, é natural que seus rastros não tenham ingressado no sistema, nesse caso, por favor, tente novamente mais tarde. Adicionalmente,' \
            'verifique se o código digitado está correto:' \
            'LZ428144337US' \
            '<input type=hidden name="Z_ACTION">' \
            '<input type=button name="done" value="Back" onClick="BT_VOLTAR_OnClick(this)">' \
            '</FORM>' \
            '</BODY>'

        self.mock_http.return_value = s
        self.assertListEqual([], PyTrackingBR.get_tracking(self.cod_for_tracking))

    def test_tracking_with_observation(self):
        s = '<table  border cellpadding=1 hspace=10>' \
            '<colgroup style=\'font:8pt Tahoma;color=Black\' valign=top><colgroup style=\'font:8pt Tahoma; color=Navy\'><colgroup style=\'font:8pt Tahoma;color=Maroon\'>' \
            '<tr>' \
            '<td><font FACE=Tahoma color=\'#CC0000\' size=2><b>Data</b></font></td>' \
            '<td><font FACE=Tahoma color=\'#CC0000\' size=2><b>Local</b></font></td>' \
            '<td><font FACE=Tahoma color=\'#CC0000\' size=2><b>Situação</b></font></td>' \
            '</tr>' \
            '<tr><td rowspan=2>28/02/2014 07:05</td><td>ESTADOS UNIDOS</td><td><FONT COLOR="000000">Encaminhado</font></td></tr>' \
            '<tr><td colspan=2>Em trânsito para UNIDADE DE TRATAMENTO INTERNACIONAL - BRASIL</td></tr>' \
            '<tr><td rowspan=1>24/02/2014 10:26</td><td>ESTADOS UNIDOS</td><td><FONT COLOR="000000">Postado</font></td></tr>' \
            '</TABLE>'
        self.mock_http.return_value = s
        result = [{'observation': 'Em trânsito para UNIDADE DE TRATAMENTO INTERNACIONAL - BRASIL', 'time': '28/02/2014 07:05 AM', 'status': 'Encaminhado', 'location': 'ESTADOS UNIDOS'}, {'time': '24/02/2014 10:26 AM', 'status': 'Postado', 'location': 'ESTADOS UNIDOS'}]
        self.assertListEqual(result, PyTrackingBR.get_tracking(self.cod_for_tracking))


if __name__ == '__main__':
    unittest.main()
