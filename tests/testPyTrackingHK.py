#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

from tests.utils import unittest
from tests.utils import mock
from ptracking.ptracking_utils import PyTrackingHK


class TestPyTrackingHK(unittest.TestCase):
    def setUp(self):
        self.patcher = mock.patch('ptracking.ptracking_utils.PyTrackingUtils.get_html')
        self.mock_http = self.patcher.start()
        self.cod_for_tracking = "RB993299068HK"

    def tearDown(self):
        self.patcher.stop()

    def test_format_date(self):
        self.assertEqual("27/04/2014", PyTrackingHK._PyTrackingHK__format_date("27-apr-2014"))

    def test_simple_tracking(self):
        s = '<p><span class="textNormalBlack">Destination</span> - Brazil</p>' \
            'The item (RB993299068HK) arrived at Brazil on 23-Mar-2014.<br /><br />'
        self.mock_http.return_value = s
        result = [{"time": "23/03/2014",
                   "status": "arrived at Brazil",
                   "observation": "Destination Brazil"}]
        self.assertListEqual(result, PyTrackingHK.get_tracking(self.cod_for_tracking))

    def test_empty_tracking(self):
        s = 'The Enquiry Reference Number (<span class="textNormalBlack">RB9932999</span>) is not available.' \
            'We are sorry that we are unable to provide you with further information.'
        self.mock_http.return_value = s
        self.assertListEqual([], PyTrackingHK.get_tracking(self.cod_for_tracking))


if __name__ == '__main__':
    unittest.main()
