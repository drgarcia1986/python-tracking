#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

from tests.utils import unittest
from tests.utils import mock
from ptracking.ptracking_utils import PyTrackingUSPS


class TestPyTrackingUSPS(unittest.TestCase):
    def setUp(self):
        self.patcher = mock.patch('ptracking.ptracking_utils.PyTrackingUtils.get_html')
        self.mock_http = self.patcher.start()
        self.cod_for_tracking = "LZ428144337US"

    def tearDown(self):
        self.patcher.stop()

    def test__format_date_time(self):
        method = PyTrackingUSPS._PyTrackingUSPS__format_date_time
        self.assertEqual("27/04/2014", method("April 27, 2014, "))
        self.assertEqual("27/04/2014 07:59 PM", method("April 27, 2014, 07:59 PM"))
        self.assertEqual("27/04/2014 07:59 AM", method("April 27, 2014, 07:59 AM"))
        self.assertEqual("27/04/2014 08:02 PM", method("April 27, 2014, 20:02:30"))
        self.assertEqual("27/04/2014 08:02 AM", method("April 27, 2014, 08:02:30"))

    def test_simple_tracking(self):
        s = '<table id="scanHistoryTable" cellspacing="0">' \
            '<tr bgcolor="#E6E7E8">' \
            '<td class="scanHistoryTopRow scanHistoryLeftCol"><strong>Date</strong></td>' \
            '<td class="scanHistoryTopRow"><strong>Time</strong></td>' \
            '<td class="scanHistoryTopRow"><strong>Location</strong></td>' \
            '<td class="scanHistoryTopRow scanHistoryRightCol"><strong>Status</strong></td>' \
            '</tr>' \
            '<tr bgcolor="#F1F2F2">' \
            '<td class="scanHistoryLeftCol">February 28, 2014</td>' \
            '<td>7:05 am</td>' \
            '<td>' \
            'ISC MIAMI FL (USPS),' \
            '</td>' \
            '<td class="scanHistoryRightCol">Processed Through Sort Facility</td>' \
            '</tr>' \
            '</table>'
        self.mock_http.return_value = s
        result = [{"time": "28/02/2014 07:05 AM",
                  "location": "ISC MIAMI FL USPS",
                  "status": "Processed Through Sort Facility"}]
        self.assertListEqual(result, PyTrackingUSPS.get_tracking(self.cod_for_tracking))

    def test_empty_tracking(self):
        s = '<p id="errorMessage">The confirmation number entered was not found. ' \
            '<p>Please check your number and try again.  Please note that confirmation numbers are only ' \
            'retained for six (6) months.</p>'
        self.mock_http.return_value = s
        self.assertListEqual([], PyTrackingUSPS.get_tracking(self.cod_for_tracking))

    def test_tracking_with_observation(self):
        s = '<tr bgcolor="#E6E7E8">' \
            '<td class="scanHistoryLeftCol">February 27, 2014</td>' \
            '<td>3:12 pm</td>' \
            '<td>' \
            'ISC MIAMI FL (USPS),' \
            '</td>' \
            '<td class="scanHistoryRightCol">Arrived at Sort Facility</td>' \
            '</tr>' \
            '<tr bgcolor="#F1F2F2">' \
            '<td class="scanHistoryLeftCol">February 24, 2014</td>' \
            '<td></td>' \
            '<td>' \
            '</td>' \
            '<td class="scanHistoryRightCol">Electronic Shipping Info Received</td>' \
            '</tr>' \
            '<tr bgcolor="#E6E7E8">' \
            '<td class="scanHistoryLeftCol">February 24, 2014</td>' \
            '<td>10:26 am</td>' \
            '<td>' \
            'TAMPA,' \
            'FL 33603</td>' \
            '<td class="scanHistoryRightCol">Acceptance</td>' \
            '</tr></table>'
        self.mock_http.return_value = s
        result = [{"time": "27/02/2014 03:12 PM",
                  "location": "ISC MIAMI FL USPS",
                  "status": "Arrived at Sort Facility"},
                  {"time": "24/02/2014 10:26 AM",
                   "location": "TAMPA FL 33603",
                   "status": "Acceptance",
                   "observation": "Electronic Shipping Info Received"}]
        self.assertListEqual(result, PyTrackingUSPS.get_tracking(self.cod_for_tracking))

    def test_tracking_without_location_in_first(self):
        s = '<tr bgcolor="#F1F2F2">' \
            '<td class="scanHistoryLeftCol">March 20, 2014</td>' \
            '<td>1:14 pm</td>' \
            '<td>' \
            '</td>' \
            '<td class="scanHistoryRightCol">Customs Clearance</td>' \
            '</tr>'
        self.mock_http.return_value = s
        result = [{"time": "20/03/2014 01:14 PM",
                   "status": "Customs Clearance"}]
        self.assertListEqual(result, PyTrackingUSPS.get_tracking(self.cod_for_tracking))


if __name__ == '__main__':
    unittest.main()
