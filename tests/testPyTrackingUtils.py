#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

from tests.utils import unittest
from tests.utils import mock
from io import BytesIO
from ptracking.ptracking_utils import PyTrackingUtils


class TestPyTrackingUtils(unittest.TestCase):
    def test_clean_up_string(self):
        s = " something\n%$!&*@ text for()))/? test"
        self.assertEqual("something text for test", PyTrackingUtils.clean_up_string(s))

    def test_clean_up_tags(self):
        s = "<b>something</b><br \> <p>text</P> <ul>for <li>test</li></ul>"
        self.assertEqual("something text for test", PyTrackingUtils.clean_up_tags(s))
        
    def test_convert_to_12h_time_format(self):
        time = "27/04/2014 16:18"
        self.assertEqual("27/04/2014 04:18 PM", PyTrackingUtils.convert_to_12h_time_format(time))

    @mock.patch("ptracking.ptracking_utils.req.urlopen", autospec=True)
    def test_get_html(self, mock_http):
        b = BytesIO(b"<b>Test page</b>")
        mock_http.return_value = b
        url = "http://test.pytracking.com"

        self.assertEqual("<b>Test page</b>", PyTrackingUtils.get_html(url))

        mock_http.assert_called_once_with(url)


if __name__ == '__main__':
    unittest.main()
