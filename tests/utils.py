#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'
import sys

try:
    import unittest.mock as mock
except ImportError:
    import mock

if sys.version_info >= (3,):
    import unittest
else:
    try:
        import unittest2 as unittest
    except ImportError:
        import unittest